
const Sequelize = require('sequelize');

// new Sequelize instance
//                     // 'database name', 'user', 'password'
const sequelize = new Sequelize('cloudokiapi', 'postgres', 'postgres', {
  host: 'localhost',
  port: '5432',
  dialect: 'postgres',

  // http://docs.sequelizejs.com/class/lib/sequelize.js~Sequelize.html
  // search for pool options for more information
  pool: {
    // Maximum number of connection in pool
    max: 7,

    // Minimum number of connection in pool
    min: 0,

    // The maximum time, in milliseconds, that a connection can be idle before being released.
    idle: 10000,

    // The maximum time, in milliseconds, that pool will try to get connection before throwing error
    acquire: 30000,

    // The time interval, in milliseconds, after which sequelize-pool will remove idle connections.
    evict: 10000
  }

});

// checks sequelize if connection has been made, or it's unavailable
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database ::; ', err);
  });

// creates tables automatically if does not exists
sequelize.sync();

module.exports = sequelize
