
const Sequelize = require('sequelize');

const sequelize = require('../../../configs/dbConfig');

const Company = require('../../Company/model/company');

// defines sequelize instance table
const Developer = sequelize.define('developer', {
  // creating model attributes
  name: Sequelize.STRING,
  languages: Sequelize.ARRAY(Sequelize.TEXT)
});

Developer.hasOne(Company, { as: 'dev_company', foreignKey: 'developer_id' });

Company.hasMany(Developer, { constraints: false, as: 'dev_company', foreignKey: 'developer_id' });

module.exports = Developer;
