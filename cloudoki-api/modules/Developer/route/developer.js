
const express = require('express');
const router = express.Router();

const Developer = require('../controller/developer');

// router endpoint to create developer
router.post('/developer', Developer.create)

// router endpoint to update developer wallet req.params :id
// router.put('/developer/:id', Developer.updateWalletAddress)
// router endpoint to update developer wallet (get developer id by email)
// router.put('/developer', Developer.updateWalletAddress)

// router endpoint to finds all developer
// router.get('/developer', Developer.findAll)

module.exports = router
