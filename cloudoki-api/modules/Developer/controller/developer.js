
// const mwu = require('../../Utils/middlewareUtils');
// const qru = require('../../Utils/queriesUtils');

const Developer = require('../model/developer');

// function to create developer
const create = async ( req, res ) => {

	try {
		// object to pass attributes in req.body creates developer info
		/*
			Object example to pass and test in req.body as json(application/json):

			POST
			{
				"email": "erni@souza.com",
				"languages": "a1b2c3d4"
			}

			Returns
			{
		    "developer": {
		        "id": 2,
		        "email": "erni@gmail.com",
						"languages": "a1b2c3d4",
		        "ip": "::ffff:201.17.211.111",
		        "updatedAt": "2018-11-29T17:00:00.555Z",
		        "createdAt": "2018-11-29T17:00:00.555Z"
		    },
		    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImVybmlAZ21haWwuY29tIiwiaWF0IjoxNTQzNTEwODAwLCJleHAiOjE1NDM1MTQ0MDB9.aN8eotdbWELT7pknGVhg_uoCPrJe-BKhhtDn6G17YvY"
			}

			"token" is to be passed in Headers as key and value only when developer sends its score,
			 key: Authorization, value: Bearer token,
			 example: Authorization				Bearer  eyJhbGciOiJIUzI1NiI.sInR5cCI6IkpXVCJ9eyJlbWFpbCI6Im.VybmlAZ21haWwuY29tIiwiaWF0Ij

		**/
		const objDeveloper = {
			name: req.body.name || 'John Doe',
			languages: req.body.languages || ['Javascript', 'Erlang', 'Python']
		}
		console.log( 'objDeveloper ::; ', objDeveloper);

		// creates developer info in database
		const developer = await Developer.create(objDeveloper);
		console.log( 'developer ::; ', developer );

		// const payload = { email: objDeveloper.email };
		// console.log('payload ::; ', payload);

		// const token = mwu.createTokenDeveloper( payload );
		// console.log('token ::; ', token);

    // res.status(201).json({ developer: developer, token: token });
		res.status(201).json({ developer: developer });

	} catch (error) {
		res.status(400).json(error);
	}
}

// const updateWalletAddress = async ( req, res ) => {
//
// 	try {
//
// 		const languages = req.body.languages;
// 		const email = req.body.email;
// 		console.log(' req.body.languages ::; ', languages);
//
// 		const developerIdByEmail = await qru.getDeveloperIdByEmail( email )
// 		const id = req.params.id || developerIdByEmail;
// 		console.log(' req.params.id ::; ', id);
//
// 		const developerEmailById = await qru.getDeveloperEmailById( id )
// 		console.log(' developerEmailById ::; ', developerEmailById);
//
// 		const updateWallet = await qru.updateDeveloperWalletAddress( languages, id )
// 		console.log(' updateWallet ::; ', updateWallet);
//
// 		res.json({ languages: languages, email: developerEmailById, developerId: id })
//
// 	} catch (error) {
// 		res.status(400).json(error);
// 	}
//
// }

// function to find all developer
// const findAll = ( req, res ) => {
//
// 	// finds all developer info in database
// 	Developer.findAll()
// 	// finds all developer
// 	.then( developer => {
// 		// console.log( 'developer ::; ', developer);
// 		res.status(200).json(developer);
// 	})
// 	// error to create developer
// 	.catch(err => {
// 		res.status(400).json(err);
// 	})
// }

const objModulesToExport = {
	create,
	// updateWalletAddress,
	// findAll
}

module.exports = objModulesToExport
