
const Sequelize = require('sequelize');

const sequelize = require('../../../configs/dbConfig');

const Developer = require('../../Developer/model/developer');

// defines sequelize instance table
const Company = sequelize.define('company', {
  // creating model attributes
  name: Sequelize.STRING
});

module.exports = Company;
