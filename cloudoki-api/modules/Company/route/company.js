
const express = require('express');
const router = express.Router();

const Company = require('../controller/company');

// router endpoint to create company
router.post('/company', Company.create)

// router endpoint to update company wallet req.params :id
// router.put('/company/:id', Company.updateWalletAddress)
// router endpoint to update company wallet (get company id by email)
// router.put('/company', Company.updateWalletAddress)

// router endpoint to finds all company
// router.get('/company', Company.findAll)

module.exports = router
