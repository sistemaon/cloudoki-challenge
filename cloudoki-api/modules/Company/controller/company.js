
// const mwu = require('../../Utils/middlewareUtils');
// const qru = require('../../Utils/queriesUtils');

const Company = require('../model/company');

// function to create company
const create = async ( req, res ) => {

	try {
		// object to pass attributes in req.body creates company info
		/*
			Object example to pass and test in req.body as json(application/json):

			POST
			{
				"email": "erni@souza.com",
				"languages": "a1b2c3d4"
			}

			Returns
			{
		    "company": {
		        "id": 2,
		        "email": "erni@gmail.com",
						"languages": "a1b2c3d4",
		        "ip": "::ffff:201.17.211.111",
		        "updatedAt": "2018-11-29T17:00:00.555Z",
		        "createdAt": "2018-11-29T17:00:00.555Z"
		    },
		    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImVybmlAZ21haWwuY29tIiwiaWF0IjoxNTQzNTEwODAwLCJleHAiOjE1NDM1MTQ0MDB9.aN8eotdbWELT7pknGVhg_uoCPrJe-BKhhtDn6G17YvY"
			}

			"token" is to be passed in Headers as key and value only when company sends its score,
			 key: Authorization, value: Bearer token,
			 example: Authorization				Bearer  eyJhbGciOiJIUzI1NiI.sInR5cCI6IkpXVCJ9eyJlbWFpbCI6Im.VybmlAZ21haWwuY29tIiwiaWF0Ij

		**/
		const objCompany = {
			name: req.body.name || 'cloudoki'
		}
		console.log( 'objCompany ::; ', objCompany);

		// creates company info in database
		const company = await Company.create(objCompany);
		console.log( 'company ::; ', company );

		// const payload = { email: objCompany.email };
		// console.log('payload ::; ', payload);

		// const token = mwu.createTokenCompany( payload );
		// console.log('token ::; ', token);

    // res.status(201).json({ company: company, token: token });
		res.status(201).json({ company: company });

	} catch (error) {
		res.status(400).json(error);
	}
}

// const updateWalletAddress = async ( req, res ) => {
//
// 	try {
//
// 		const languages = req.body.languages;
// 		const email = req.body.email;
// 		console.log(' req.body.languages ::; ', languages);
//
// 		const companyIdByEmail = await qru.getCompanyIdByEmail( email )
// 		const id = req.params.id || companyIdByEmail;
// 		console.log(' req.params.id ::; ', id);
//
// 		const companyEmailById = await qru.getCompanyEmailById( id )
// 		console.log(' companyEmailById ::; ', companyEmailById);
//
// 		const updateWallet = await qru.updateCompanyWalletAddress( languages, id )
// 		console.log(' updateWallet ::; ', updateWallet);
//
// 		res.json({ languages: languages, email: companyEmailById, companyId: id })
//
// 	} catch (error) {
// 		res.status(400).json(error);
// 	}
//
// }

// function to find all company
// const findAll = ( req, res ) => {
//
// 	// finds all company info in database
// 	Company.findAll()
// 	// finds all company
// 	.then( company => {
// 		// console.log( 'company ::; ', company);
// 		res.status(200).json(company);
// 	})
// 	// error to create company
// 	.catch(err => {
// 		res.status(400).json(err);
// 	})
// }

const objModulesToExport = {
	create,
	// updateWalletAddress,
	// findAll
}

module.exports = objModulesToExport
